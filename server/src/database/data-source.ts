import "reflect-metadata"
import { DataSource } from "typeorm"

import User from "../models/User";
import Profile from "../models/Profile";
import Post from "../models/Post";
import Comment from "../models/Comment";
import Rely from "../models/Rely";


const AppDataSource = new DataSource({
    type: process.env.DB_TYPE as any,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT as any,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: true,
    logging: false,
    entities: [User, Profile, Post, Comment, Rely],
    migrations: [],
    subscribers: []
})


export default AppDataSource