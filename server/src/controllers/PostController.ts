import
{
    Request,
    Response
}
    from "express"

import AppDataSource from "../database/data-source"
import Post from "../models/Post";
import Comment from "../models/Comment";
import Rely from "../models/Rely";
import sendEmail from "../utils/email";


export default class PostController
{
    public static async getMany(req: Request, res: Response)
    {
        const posts = await AppDataSource
            .getRepository(Post)
            .createQueryBuilder('post')
            .innerJoinAndSelect('post.user', 'user')
            .leftJoinAndSelect('post.comments', 'comment')
            .leftJoinAndSelect('comment.replies', 'reply')
            .orderBy('post.createdAt', 'DESC')
            .getMany()

        res.status(200).send(posts)
    }


    public static async addReply(req: Request, res: Response)
    {
        const commentId = req.params.commentId

        const foundComment = await AppDataSource
            .getRepository(Comment)
            .createQueryBuilder("comment")
            .innerJoinAndSelect('comment.user', 'user')
            .where("comment.id = :id", {id: commentId})
            .getOne()

        console.log(foundComment)

        const newReply = new Rely()
        newReply.comment = foundComment
        newReply.user = req.body.user
        newReply.content = req.body.content

        await AppDataSource
            .createQueryBuilder()
            .insert()
            .into(Rely)
            .values(newReply)
            .execute()

        // let email = {
        //     to: foundComment.user.email,
        //     subject: 'some on has replied your comment',
        //     text: 'That was easy!'
        // }

        // sendEmail(email);

        res.status(201).send({message: "Created Successfully."})
    }


    public static async addComment(req: Request, res: Response)
    {
        const postId = req.params.postId

        const foundPost = await AppDataSource
            .getRepository(Post)
            .createQueryBuilder("post")
            .where("post.id = :id", {id: postId})
            .getOne()

        const newComment = new Comment()
        newComment.post = foundPost
        newComment.user = req.body.user
        newComment.content = req.body.content

        await AppDataSource
            .createQueryBuilder()
            .insert()
            .into(Comment)
            .values(newComment)
            .execute()

        res.status(201).send({message: "Created Successfully."})
    }


    public static async getOne(req: Request, res: Response)
    {
        const postId = req.params.postId

        const foundPost = await AppDataSource
            .getRepository(Post)
            .createQueryBuilder("post")
            .innerJoinAndSelect('post.user', 'user')
            .leftJoinAndSelect('post.comments', 'comment')
            .leftJoinAndSelect('comment.user', 'comment-replier')
            .leftJoinAndSelect('comment.replies', 'reply')
            .leftJoinAndSelect('reply.user', 'reply-replier')
            .where("post.id = :id", {id: postId})
            .orderBy('comment.createdAt', 'DESC')
            .orderBy('reply.createdAt', 'DESC')
            .getOne()

        if (foundPost) res.status(200).send(foundPost)
        else res.status(404).send({message: "Not found."})
    }


    public static async create(req: Request, res: Response)
    {
        await AppDataSource
            .createQueryBuilder()
            .insert()
            .into(Post)
            .values({
                title: req.body.title,
                content: req.body.content,
                user: req.body.user
            })
            .execute()

        res.status(201).send(req.body.user)
    }
}
