import
{
    Express, raw,
    Request,
    Response
}
    from "express"

import AppDataSource from "../database/data-source"
import User from "../models/User"
import Profile from "../models/Profile"


export default class UserController
{
    public static async create(req: Request, res: Response)
    {
        const user = new User()
        const profile = new Profile()

        user.profile = profile
        profile.user = user

        user.email = req.body.email
        profile.name = req.body.name

        await AppDataSource.manager.save(user)
        await AppDataSource.manager.save(profile)

        res.status(200).send({accessToken: user.accessToken})
    }


    public static async getAccessToken(req: Request, res: Response)
    {
        const foundedUser = await AppDataSource
            .getRepository(User)
            .createQueryBuilder("user")
            .where("user.email")
    }
}