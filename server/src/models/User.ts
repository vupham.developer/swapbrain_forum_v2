import
{
    Entity,
    Column,
    PrimaryGeneratedColumn,
    Generated,
    CreateDateColumn,
    UpdateDateColumn,
    OneToOne,
    OneToMany
}
    from "typeorm"

import Profile from "./Profile"
import Post from "./Post"


@Entity()
export default class User
{
    @PrimaryGeneratedColumn("uuid")
    id: number

    @Column({ type: "varchar", length: 100, unique: true })
    email: string

    @Column({ unique: true })
    @Generated("uuid")
    accessToken: string

    @OneToOne(() => Profile)
    profile: Profile

    @OneToMany(() => Post, (post: Post) => post.user)
    posts: Post[]

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date
}