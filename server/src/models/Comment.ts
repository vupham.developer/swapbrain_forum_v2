import
{
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany
}
    from "typeorm"

import Post from "./Post"
import Rely from "./Rely"
import User from "./User"


@Entity()
export default class Comment
{
    @PrimaryGeneratedColumn("uuid")
    id: number

    @Column({ type: "text" })
    content: string

    @ManyToOne(() => Post,
        (post: Post) => post.comments)
    post: Post

    @ManyToOne(() => User, (user: User) => user.posts)
    user: User

    @OneToMany(() => Rely, (reply: Rely) => reply.comment)
    replies: Rely[]

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date
}