import
{
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn
}
    from "typeorm"

import User from "./User"


@Entity()
export default class Profile
{
    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: "varchar", length: 150 })
    name: string

    @OneToOne(() => User)
    @JoinColumn()
    user: User

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date
}