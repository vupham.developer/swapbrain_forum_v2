import
{
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne, CreateDateColumn, UpdateDateColumn
}
    from "typeorm"

import Comment from "./Comment";
import User from "./User";


@Entity()
export default class Rely
{
    @PrimaryGeneratedColumn("uuid")
    id: number

    @Column({ type: "text" })
    content: string

    @ManyToOne(() => Comment, (comment: Comment) => comment.replies)
    comment: Comment

    @ManyToOne(() => User, (user: User) => user.posts)
    user: User

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date
}