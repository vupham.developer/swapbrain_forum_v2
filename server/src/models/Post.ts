import
{
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToMany
}
    from "typeorm"


import User from "./User"
import Comment from "./Comment";


@Entity()
export default class Post
{
    @PrimaryGeneratedColumn("uuid")
    id: number

    @Column({ type: "text" })
    title: string

    @Column({ type: "text" })
    content: string

    @ManyToOne(() => User, (user: User) => user.posts)
    user: User

    @OneToMany(() => Comment, (comment: Comment) => comment.post)
    comments: Comment[]

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date
}