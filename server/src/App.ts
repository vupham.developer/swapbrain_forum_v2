import express, {Express} from "express"
import cors from "cors"
import bodyParser from "body-parser"
import path from "path"

import Api from "./routes/Api"
import AppDataSource from "./database/data-source"


class App
{
    private readonly app: Express
    private reactBuild = path.join(__dirname, 'react_build')

    constructor()
    {
        this.app = express()

        this.app.use(express.static(this.reactBuild))
        this.app.use(bodyParser())
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded())
        this.app.use(bodyParser.urlencoded({ extended: true }))
        this.app.use(cors())


        Api(this.app) // Register API routes.

        this.app.get('*', async (req: express.Request, res: express.Response) => {
            res.sendFile(path.join(this.reactBuild, 'index.html'))
        })

    }

    /**
     * @desc - Call to run the server.
     */
    run(port: number)
    {
        this.app.listen(port, async () => {
            await AppDataSource.initialize() // Connect to database.
        })
    }


    /**
     * @desc - It's like the main function.
     */
    static {
        const port = process.env.APP_PORT as any

        const app = new App()
        app.run(port)
    }
}
