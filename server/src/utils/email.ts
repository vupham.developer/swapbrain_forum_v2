import nodemailer from "nodemailer"


const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_HOST,
        pass: process.env.EMAIL_PASSWORD
    }
})


export default function sendEmail(mail)
{
    mail.from = process.env.EMAIL_HOST

    transporter.sendMail(mail, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}


// export default transporter;
