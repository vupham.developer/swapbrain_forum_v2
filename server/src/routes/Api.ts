import express, {Express} from "express";

import UserController from "../controllers/UserController";
import PostController from "../controllers/PostController";
import authenticate from "../middleware/Authenticate";
import path from "path";


export default function Api(app: Express)
{
    const apiEndpoint = "/api"


    // UserController.
    app.post(`${apiEndpoint}/users/login`, UserController.getAccessToken)

    app.post(`${apiEndpoint}/users`, UserController.create)


    // PostController.
    app.get(`${apiEndpoint}/posts`, PostController.getMany)

    app.get(`${apiEndpoint}/posts/:postId`, PostController.getOne)

    app.post(`${apiEndpoint}/posts`, authenticate, PostController.create)

    app.post(`${apiEndpoint}/comments/:commentId/replies`, authenticate, PostController.addReply)

    app.post(`${apiEndpoint}/posts/:postId/comments`, authenticate, PostController.addComment)
}
