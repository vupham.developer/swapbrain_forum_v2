import {NextFunction, Request, Response} from "express"

import AppDataSource from "../database/data-source"
import User from "../models/User"


export default async function authenticate
(
    req: Request,
    res: Response,
    next: NextFunction
)
{
    const authEmail = req.query.email


    if (authEmail)
    {
        let foundUser = await AppDataSource
            .getRepository(User)
            .createQueryBuilder("user")
            .where("user.email = :email", {email: authEmail})
            .getOne()

        if (!foundUser){
            console.log("hahahahah")

            foundUser = new User()
            foundUser.email = String(authEmail)

            await AppDataSource
                .createQueryBuilder()
                .insert()
                .into(User)
                .values(foundUser)
                .execute()
        }

        req.body.user = foundUser

        next()
    }
    else
    {
        res.status(403).send({message: "Unauthorized."})
    }
}
