# Overview

This repository is The Forum of our product "PinyinTube extention" which is help users can discuss together, raise problems and help us to evolve our extention
This repository include both frontend and backend side


# Main Technical

- Frontend:
    + ReactJS - Typescript
    + TailwindCss (In the next version we will try to bring all inline TailwindCss to the CSS file)
    + Axios

- Backend:
    + ExpressJS - Typescript
    + Typeorm


# How to run the project

## 1 clone the repository

## 2 run `npm install` in the both `client` and `server` folder

## 3 create a new database

## 4 go to `server/.env` and edit the database config to fit with your local database, then run `npm start` and coppy the url of where the server run

## 5 go to `client/.env` and adjust  `REACT_APP_API_ENDPOINT` to the url that you have coppied then run `npm start`

## 6 now you can see the server
